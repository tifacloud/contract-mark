package com.tifa.mark.controller;

import com.tifa.mark.pojo.Contract;
import com.tifa.mark.service.UploadService;
import javax.annotation.Resource;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class NextController {

  @Resource
  private UploadService uploadService;

  @RequestMapping("/selectOne")
  public Contract selectOne(Model model) {
    Contract contract = uploadService.selectOne();
    String art = contract == null ? "完成了" : contract.getText();
    return contract;
  }
}
