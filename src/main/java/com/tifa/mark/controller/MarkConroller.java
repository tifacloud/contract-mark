package com.tifa.mark.controller;

import com.tifa.mark.pojo.Contract;
import com.tifa.mark.service.UploadService;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Controller
public class MarkConroller {

  @Resource
  private UploadService uploadService;

  @RequestMapping("/upload")
  public String upLoad(MultipartHttpServletRequest request, Model model) throws IOException {
    List<MultipartFile> files = request.getFiles("file");
    uploadService.insertContract(files);
    Contract contract = uploadService.selectOne();
    String art = contract == null ? "完成了" : contract.getText();
    model.addAllAttributes(Map.of("art", art));
    return "index";
  }

  @RequestMapping("/next")
  public String nextContract(@RequestParam("text") String text,
      @RequestParam(value = "id", required = false) Integer id) {
    if (text == null || text.length() == 0) {
      uploadService.deleteContractById(id);
      return "forward:/selectOne";
    }
    uploadService.insertEntity( text, id);
    return "forward:/selectOne";
  }

  @RequestMapping("begin_mark")
  public String beginMark(Model model) {
    Contract contract = uploadService.selectOne();
    String art = contract == null ? "完成了" : contract.getText();
    model.addAllAttributes(Map.of("art", art));
    return "index";
  }

}
