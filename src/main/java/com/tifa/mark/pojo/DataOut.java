package com.tifa.mark.pojo;

public class DataOut {

  private Integer id;

  private Integer contractId;

  private Integer paraIndex;

  private String entity;

  private Integer paraLength;

  private Integer label;

  public Integer getLabel() {
    return label;
  }

  public void setLabel(Integer label) {
    this.label = label;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public Integer getContractId() {
    return contractId;
  }

  public void setContractId(Integer contractId) {
    this.contractId = contractId;
  }

  public Integer getParaIndex() {
    return paraIndex;
  }

  public void setParaIndex(Integer paraIndex) {
    this.paraIndex = paraIndex;
  }

  public String getEntity() {
    return entity;
  }

  public void setEntity(String entity) {
    this.entity = entity;
  }

  public Integer getParaLength() {
    return paraLength;
  }

  public void setParaLength(Integer paraLength) {
    this.paraLength = paraLength;
  }

  @Override
  public String toString() {
    return "DataOut{" +
        "id=" + id +
        ", contractId=" + contractId +
        ", paraIndex=" + paraIndex +
        ", entity='" + entity + '\'' +
        ", paraLength=" + paraLength +
        '}';
  }
}
