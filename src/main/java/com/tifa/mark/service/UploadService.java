package com.tifa.mark.service;

import com.tifa.mark.pojo.Contract;
import java.io.IOException;
import java.util.List;
import org.springframework.lang.Nullable;
import org.springframework.web.multipart.MultipartFile;

public interface UploadService {

  boolean insertContract(List<MultipartFile> files) throws IOException;


  @Nullable
  Contract selectOne();


  Contract getContractById(Integer id);

  boolean insertEntity( String text, Integer id);

  void deleteContractById(Integer id);
}
