package com.tifa.mark.service.impl;

import com.tifa.mark.dao.ContractDao;
import com.tifa.mark.pojo.Contract;
import com.tifa.mark.pojo.EntityContract;
import com.tifa.mark.service.UploadService;
import com.tifa.mark.utils.ReadWord;
import com.tifa.mark.utils.Utils;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.Resource;
import org.apache.commons.lang3.StringUtils;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UploadServiceImpl implements UploadService {

  @Resource
  ContractDao contractDao;

  @Override
  @Transactional
  public boolean insertContract(List<MultipartFile> files) throws IOException {
    List<Contract> result = new ArrayList<>();

    for (MultipartFile multipartFile : files) {
      if (multipartFile.isEmpty()) {
        continue;
      }
      var name = multipartFile.getOriginalFilename();
      if (name == null) {
        continue;
      }
      if (!name.endsWith("doc") && !name.endsWith("docx")) {
        continue;
      }

      String wordText = ReadWord.getWordText(multipartFile.getInputStream(), name);
      String str = Utils.splitText(wordText);
      Contract contract = new Contract();
      contract.setText(str);
      contract.setParaLength(str.split(StringUtils.LF).length);
      contract.setName(new File(multipartFile.getOriginalFilename()).getName());
      result.add(contract);
    }
    return contractDao.insertContract(result);
  }

  @Override
  @Nullable
  public Contract selectOne() {
    return contractDao.selectWhereNotMark();
  }

  @Override
  @Transactional
  public Contract getContractById(Integer id) {
    return contractDao.getContractById(id);
  }


  @Override
  @Transactional
  public void deleteContractById(Integer id) {
    contractDao.deleteContractById(id);
  }

  @Override
  public boolean insertEntity(String text, Integer id) {
    if (id == null) {
      return false;
    }
    List<EntityContract> result = new ArrayList<>();
    Contract contract = contractDao.getContractById(id);
    var originalText = contract.getText();
    int start = StringUtils.indexOf(originalText, text);
    int end = start + text.length();

    String preString = originalText.substring(0, start);

    int preCount = StringUtils.countMatches(preString, StringUtils.LF);
    String lastString = originalText.substring(0, end);
    int lastCount = StringUtils.countMatches(lastString, StringUtils.LF);
    String[] split = originalText.split(StringUtils.LF);

    for (int j = preCount; j <= lastCount; j++) {
      EntityContract entityContract = new EntityContract();
      entityContract.setEntity(split[j]);
      entityContract.setParaIndex(j + 1);
      entityContract.setContractId(id);
      result.add(entityContract);

    }
    contractDao.insertEntityContract(result);
    return true;

  }


}
