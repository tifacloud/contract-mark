package com.tifa.mark.dao.impl;


import com.tifa.mark.dao.ContractDao;
import com.tifa.mark.mapper.ContractMapper;
import com.tifa.mark.mapper.EntityContractMapper;
import com.tifa.mark.pojo.Contract;
import com.tifa.mark.pojo.EntityContract;
import java.util.List;
import javax.annotation.Resource;
import org.springframework.stereotype.Repository;

@Repository
public class ContractDaoImpl implements ContractDao {

  @Resource
  private ContractMapper contractMapper;

  @Resource
  private EntityContractMapper entityContractMapper;

  @Override
  public boolean insertEntityContract(List<EntityContract> entityContractList) {
    entityContractList.forEach(entityContractMapper::insertSelective);
    return true;
  }

  @Override
  public Contract selectWhereNotMark() {
    List<Contract> contracts = contractMapper.selectWhereNotMark();
    if (contracts.isEmpty()) {
      return null;
    }
    return contracts.get(0);
  }

  @Override
  public void deleteContractById(Integer id) {
    contractMapper.deleteByPrimaryKey(id);
  }

  @Override
  public Contract getContractById(Integer id) {
    return contractMapper.selectByPrimaryKey(id);
  }

  @Override
  public boolean insertContract(List<Contract> result) {
    result.forEach(contractMapper::insertSelective);
    return true;
  }
}
