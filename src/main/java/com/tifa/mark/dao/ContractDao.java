package com.tifa.mark.dao;

import com.tifa.mark.pojo.Contract;
import com.tifa.mark.pojo.EntityContract;
import java.util.List;

public interface ContractDao {


  boolean insertEntityContract(List<EntityContract> entityContractList);

  Contract selectWhereNotMark();

  Contract getContractById(Integer id);

  void deleteContractById(Integer id);

  boolean insertContract(List<Contract> result);
}
