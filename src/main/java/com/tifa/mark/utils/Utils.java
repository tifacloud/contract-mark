package com.tifa.mark.utils;

import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

public class Utils {

  /**
   * 去除空行
   *
   * @param word
   * @return
   */
  public static String splitText(String word) {
    word = StringUtils.replace(word, ":", "：");
    word = StringUtils.replace(word, "(", "（");
    word = StringUtils.replace(word, ")", "）");
    word = StringUtils.replace(word, "．", ".");

    String[] split = word.split("\r\n");
    if (split.length == 1) {
      split = word.split("\n");
    }

    List<String> paragraphs = new ArrayList<>(split.length);
    for (int i = 0; i < split.length; i++) {
      //去除空格
      split[i] = split[i].replaceAll("＿|_| |　|\uE80B", StringUtils.SPACE)
          .replace(" ", StringUtils.SPACE)
          .replaceAll("：[ |　]*：", "：");
      String s = StringUtils.trimToEmpty(split[i]);
      if (s.length() == 0) {
        continue;
      }
      paragraphs.add(
          split[i].replaceAll(" |　|_|＿|\uE80B|\\s*", StringUtils.EMPTY));
    }
    StringBuilder sb = new StringBuilder();
    for (var a : paragraphs) {
      sb.append(a);
      sb.append("\n");
    }

    return sb.toString();
  }


}
