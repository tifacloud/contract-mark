import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.tifa.mark.MarkApplication;
import com.tifa.mark.mapper.ContractMapper;
import com.tifa.mark.mapper.EntityContractMapper;
import com.tifa.mark.pojo.Contract;
import com.tifa.mark.pojo.DataOut;
import com.tifa.mark.pojo.EntityContract;
import com.tifa.mark.pojo.EntityContractExample;
import com.tifa.mark.pojo.EntityContractExample.Criteria;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Random;
import javax.annotation.Resource;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;

@SpringBootTest(classes = MarkApplication.class, webEnvironment = WebEnvironment.RANDOM_PORT)
class ContractTest {

  @Resource
  ContractMapper contractMapper;

  @Resource
  EntityContractMapper entityContractMapper;


  @Test
  public void createTrainDataSet() throws IOException {
    EntityContractExample entityContractExample = new EntityContractExample();
    Criteria criteria = entityContractExample.createCriteria();
    criteria.andContractIdIsNotNull();
    List<DataOut> result = new ArrayList<>();
    List<EntityContract> entityContracts = entityContractMapper.selectByExampleWithBLOBs(
        entityContractExample);
    entityContracts.forEach(a -> {
      var dataDB = new DataOut();
      result.add(dataDB);
      dataDB.setContractId(a.getContractId());
      dataDB.setEntity(a.getEntity());
      dataDB.setLabel(1);
      dataDB.setParaIndex(a.getParaIndex());
      Contract contract = contractMapper.selectByPrimaryKey(a.getContractId());
      dataDB.setParaLength(contract.getParaLength());
    });
    ObjectMapper objectMapper = new ObjectMapper();
    String s = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
    BufferedWriter bufferedWriter = new BufferedWriter(
        new FileWriter("C:\\Users\\hengy\\Desktop\\true.txt"));
    bufferedWriter.write(s);
    bufferedWriter.close();
  }

  @Test
  /**
   * 生成测试集。
   * 找到每份合同的量。如果只有两个。那就从前面找一个，后面找一个。
   * 找到的数量要大于等于训练集合
   */
  public void createValFile() throws IOException {
    EntityContractExample entityContractExample = new EntityContractExample();
    Criteria criteria = entityContractExample.createCriteria();
    criteria.andContractIdIsNotNull();
    List<DataOut> result = new ArrayList<>();
    List<EntityContract> entityContracts = entityContractMapper.selectByExampleWithBLOBs(
        entityContractExample);
    Integer contractId = entityContracts.stream()
        .max(Comparator.comparingInt(EntityContract::getContractId)).get().getContractId();
    int id = 0;
    boolean flag = false;
    while (id++ <= contractId) {
      System.out.println("打印id"+id);
      flag = !flag;
      entityContractExample = new EntityContractExample();
      criteria = entityContractExample.createCriteria();
      criteria.andContractIdEqualTo(id);
      List<EntityContract> entityContractsTemp = entityContractMapper.selectByExample(
          entityContractExample);
      int counts = entityContractsTemp.size();
      Contract contract = contractMapper.selectByPrimaryKey(id);
      List<DataOut> temp = new ArrayList<>();
      if (counts == 0) {
      } else {
        if (counts % 2 == 1) {
          ++counts;
        }
        var entityList = preList(entityContractsTemp, counts, contract,
            new ArrayList<>());
        entityList.addAll(flag ? lastList(entityContractsTemp, counts, contract,
            new ArrayList<>()) : randomList(entityContractsTemp, counts, contract,
            new ArrayList<>()));
        entityList.forEach(a -> {
          var dataDB = new DataOut();
          temp.add(dataDB);
          dataDB.setContractId(a.getContractId());
          dataDB.setEntity(a.getEntity());
          dataDB.setLabel(0);
          dataDB.setParaIndex(a.getParaIndex());
          Contract tempContract = contractMapper.selectByPrimaryKey(a.getContractId());
          dataDB.setParaLength(tempContract.getParaLength());
          result.add(dataDB);
        });
      }
    }
    ObjectMapper objectMapper = new ObjectMapper();
    String s = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
    BufferedWriter bufferedWriter = new BufferedWriter(
        new FileWriter("C:\\Users\\hengy\\Desktop\\false.txt"));
    bufferedWriter.write(s);
    bufferedWriter.close();
  }

  private List<EntityContract> preList(List<EntityContract> entityContractsTemp, int counts,
      Contract contract, List<EntityContract> temp) {
    var minEntity = entityContractsTemp.stream()
        .min(Comparator.comparingInt(EntityContract::getParaIndex)).get();
    int c = minEntity.getParaIndex()-1;
    int indexCount = counts/2;
    while (--c >= 0 && --indexCount >= 0) {
      EntityContract entityContract = new EntityContract();
      entityContract.setContractId(minEntity.getContractId());
      entityContract.setEntity(contract.getText().split("\n")[c]);
      entityContract.setParaIndex(c);
      temp.add(entityContract);
    }
    return temp;
  }

  private List<EntityContract> lastList(List<EntityContract> entityContractsTemp, int counts,
      Contract contract, List<EntityContract> temp) {
    var maxEntity = entityContractsTemp.stream()
        .max(Comparator.comparingInt(EntityContract::getParaIndex)).get();
    int c = maxEntity.getParaIndex();
    int indexCount = 1;
    while (++indexCount <= counts/2&&c+indexCount<contract.getParaLength()) {
      EntityContract entityContract = new EntityContract();
      entityContract.setContractId(maxEntity.getContractId());
      entityContract.setEntity(contract.getText().split("\n")[c+indexCount]);
      entityContract.setParaIndex(c+indexCount);
      temp.add(entityContract);
    }
    return temp;
  }

  private List<EntityContract> randomList(List<EntityContract> entityContractsTemp, int counts,
      Contract contract, List<EntityContract> temp) {
    var maxEntity = entityContractsTemp.stream()
        .max(Comparator.comparingInt(EntityContract::getParaIndex)).get();

    var minEntity = entityContractsTemp.stream()
        .min(Comparator.comparingInt(EntityContract::getParaIndex)).get();
    int indexCount = 0;
    Random random = new Random();
    random.setSeed(47);
    while (++indexCount <= counts) {

      int origin = maxEntity.getParaIndex() + 1;
      int bound = origin > contract.getParaLength() ? origin : contract.getParaLength();
      if (bound <= origin) {
        origin = 0;
        bound = minEntity.getParaIndex();
      }

     int c= random.nextInt(origin,bound);

      EntityContract entityContract = new EntityContract();
      entityContract.setContractId(maxEntity.getContractId());
      entityContract.setEntity(contract.getText().split("\n")[c]);
      entityContract.setParaIndex(c+indexCount);
      temp.add(entityContract);
    }
    return temp;
  }
}