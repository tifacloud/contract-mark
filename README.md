# ContractMark

#### 介绍
一款基于SpringBoot+Mybatis的用来标记合同主体的标注软件，前端界面是随便写的，准备学一学VUE。

#### 安装教程

1. Maven 安装依赖即可。
2. 请注意在src/main/resources/application.yml 更换数据库的用户名和密码。

#### 使用说明

1.  运行MarkApplication。
2.  访问localhost:8888/index 

首次使用要先上传doc/docx的文件，支持文件夹上传。上传后会在 textarea 控件内显示一篇内容。在内容上画出主体所在的范围，然后点击 “下一个” 按钮即可。

